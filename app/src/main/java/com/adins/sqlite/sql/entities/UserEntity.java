package com.adins.sqlite.sql.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.adins.sqlite.model.Address;
import com.adins.sqlite.model.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import static com.adins.sqlite.sql.entities.UserEntity.TABLE_NAME;

/**
 * Created by kusnendi on 4/3/18.
 */
@Entity(tableName = TABLE_NAME)
public class UserEntity {

    public static final String TABLE_NAME = "USERS";

    @PrimaryKey
    private int id;
    private String name;
    private String username;
    private String email;
    private String phone;
    private String website;

    public UserEntity(int id, String name, String username, String email, String phone, String website) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.email = email;
        this.phone = phone;
        this.website = website;
    }

    public UserEntity(User user) {
        this.id         = user.getId();
        this.name       = user.getName();
        this.username   = user.getUsername();
        this.email      = user.getEmail();
        this.phone      = user.getPhone();
        this.website    = user.getWebsite();


    }

    public static List<UserEntity> userEntities(List<User> users) {
        List<UserEntity> entities = new ArrayList<>();
        for (User user : users) {
            entities.add(new UserEntity(user));
        }

        return entities;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }
}
