package com.adins.sqlite.sql.dao;

import android.content.Context;
import android.util.Log;

import com.adins.sqlite.common.Worker;
import com.adins.sqlite.sql.SQLHelper;
import com.adins.sqlite.sql.dao.interfaces.UserDao;
import com.adins.sqlite.sql.entities.UserEntity;

import java.util.List;

/**
 * Created by developer on 1/13/18.
 */

public class UserDaoImpl implements UserDao {
    private SQLHelper db;

    public UserDaoImpl(Context context) {
        db = SQLHelper.getInstance(context);
    }

    @Override
    public UserEntity find(int id) {
        return db.userDao().find(id);
    }



    @Override
    public void insert(final UserEntity user) {
        new Worker(new Worker.OnInitialize() {
            @Override
            public Object doWork() {
                db.userDao().insert(user);
                return null;
            }

            @Override
            public void onComplete(Object o) {
                Log.i("INFO", "Data has been inserted!");
            }
        });
    }

    @Override
    public void insertAll(final List<UserEntity> users) {
        new Worker(new Worker.OnInitialize() {
            @Override
            public Object doWork() {
                db.userDao().insertAll(users);
                return null;
            }

            @Override
            public void onComplete(Object o) {
                Log.i("INFO", "Data has been inserted!");
            }
        });
    }

    @Override
    public void update(final UserEntity user) {
        new Worker(new Worker.OnInitialize() {
            @Override
            public Object doWork() {
                db.userDao().update(user);
                return null;
            }

            @Override
            public void onComplete(Object o) {
                Log.i("INFO", "Data has been updated!");
            }
        });
    }

    @Override
    public void delete(final UserEntity user) {
        new Worker(new Worker.OnInitialize() {
            @Override
            public Object doWork() {
                db.userDao().delete(user);
                return null;
            }

            @Override
            public void onComplete(Object o) {
                Log.i("INFO", "Data has been deleted!");
            }
        });
    }
}
