package com.adins.sqlite.sql;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.adins.sqlite.sql.dao.interfaces.UserDao;
import com.adins.sqlite.sql.entities.UserEntity;

import static com.adins.sqlite.sql.SQLHelper.DB_VERSION;

/**
 * Created by kusnendi on 4/3/18.
 */

@Database(entities = {UserEntity.class}, version = DB_VERSION)
public abstract class SQLHelper extends RoomDatabase {
    public static final int DB_VERSION  = 1;
    private static final String DB_NAME = "default";
    private static SQLHelper database;

    public static SQLHelper getInstance(Context context) {
        database = Room.databaseBuilder(context, SQLHelper.class, DB_NAME)
                .build();

        return database;
    }

    //User Dao
    public abstract UserDao userDao();
}
