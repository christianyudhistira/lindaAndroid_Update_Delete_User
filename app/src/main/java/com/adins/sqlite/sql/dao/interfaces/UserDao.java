package com.adins.sqlite.sql.dao.interfaces;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.adins.sqlite.sql.entities.UserEntity;

import java.util.List;

/**
 * Created by kusnendi on 4/3/18.
 */

@Dao
public interface UserDao {
    @Query("SELECT * FROM " + UserEntity.TABLE_NAME + " WHERE id = :id")
    public UserEntity find(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insert(UserEntity user);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertAll(List<UserEntity> users);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public void update(UserEntity user);

    @Delete
    public void delete(UserEntity user);
}
