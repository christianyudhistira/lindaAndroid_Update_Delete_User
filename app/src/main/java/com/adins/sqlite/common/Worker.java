package com.adins.sqlite.common;

import android.os.AsyncTask;

/**
 * Created by developer on 11/4/17.
 */

public class Worker {
    //MARK: Properties
    private OnInitialize callback;

    //Initialize Worker Instance
    public Worker(OnInitialize call) {
        this.callback = call;
        new AsyncTask<Void, Void, Object>() {
            @Override
            protected void onPostExecute(Object result) {
                super.onPostExecute(result);
                callback.onComplete(result);
            }

            @Override
            protected Object doInBackground(Void... voids) {
                return callback.doWork();
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public static interface OnInitialize {
        public Object doWork();
        public void onComplete(Object o);
    }
}
