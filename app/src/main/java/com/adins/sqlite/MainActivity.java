package com.adins.sqlite;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.adins.sqlite.adapter.UserAdapter;
import com.adins.sqlite.http.HttpService;
import com.adins.sqlite.model.User;
import com.adins.sqlite.sql.dao.UserDaoImpl;
import com.adins.sqlite.sql.entities.UserEntity;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    RecyclerView listView;
    UserAdapter userAdapter;
    List<User> users = new ArrayList<>();

    // define broadcast receiver
    private final BroadcastReceiver mybroadcast = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            int type = intent.getIntExtra(EditItemActivity.EXTRA_ID, 0);
            if (type == 1) { // udpate button
                String name = intent.getStringExtra(EditItemActivity.EXTRA_NAME);
                String email = intent.getStringExtra(EditItemActivity.EXTRA_EMAIL);
                String phone = intent.getStringExtra(EditItemActivity.EXTRA_PHONE);
                int position = intent.getIntExtra(EditItemActivity.EXTRA_POSITION, 0);

                userAdapter.update(position, name, email, phone);
            }
            else if (type == 2) { // delete button
                int position = intent.getIntExtra(EditItemActivity.EXTRA_POSITION, 0);
                userAdapter.delete(position);
            }
            else if (type == 3) {
                String name = intent.getStringExtra(EditItemActivity.EXTRA_NAME);
                String email = intent.getStringExtra(EditItemActivity.EXTRA_EMAIL);
                String phone = intent.getStringExtra(EditItemActivity.EXTRA_PHONE);
                userAdapter.add(name, email, phone);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        userAdapter = new UserAdapter(this, users);

        listView = findViewById(R.id.listView);
        listView.setLayoutManager(new LinearLayoutManager(this));
        listView.setHasFixedSize(true);
        listView.setAdapter(userAdapter);

        // register broadcast receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(
                mybroadcast, new IntentFilter(EditItemActivity.ACTION_USER_BROADCAST)
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        Toast.makeText(this, String.valueOf(users.size()) + " left", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        Toast.makeText(this, "DESTROY", Toast.LENGTH_LONG).show();
        // unregister broadcast receiver
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mybroadcast);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_actions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case R.id.optDownload:
                fetchData();
//                Toast.makeText(this, "Download has clicked!", Toast.LENGTH_LONG).show();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public void fetchData() {
        Retrofit retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .build();

        HttpService httpService = retrofit.create(HttpService.class);
        httpService.listUsers().enqueue(new Callback<User[]>() {
            @Override
            public void onResponse(Call<User[]> call, Response<User[]> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    users.clear();
                    users.addAll(Arrays.asList(response.body()));

                    //Notify adapter data has changed
                    userAdapter.notifyDataSetChanged();

                    //Save data to SQLite
                    UserDaoImpl userDao = new UserDaoImpl(MainActivity.this);
                    userDao.insertAll(UserEntity.userEntities(users));
                }
            }

            @Override
            public void onFailure(Call<User[]> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
