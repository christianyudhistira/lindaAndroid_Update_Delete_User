package com.adins.sqlite.http;


import com.adins.sqlite.model.User;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by kusnendi on 4/2/18.
 */

public interface HttpService {
//    @GET("exampledocs/books.json")
//    Call<Book[]> listBooks();

    @GET("users")
    Call<User[]> listUsers();
}
