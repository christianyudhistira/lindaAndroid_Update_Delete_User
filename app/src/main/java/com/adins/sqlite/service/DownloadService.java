package com.adins.sqlite.service;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.widget.Toast;


import com.adins.sqlite.http.HttpService;
import com.adins.sqlite.model.User;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by kusnendi on 4/3/18.
 */

public class DownloadService extends IntentService {
    public static final String NOTIFICATION = "com.adins.intentservice.service.DownloadService";
    public static final String DATA = "data";
    public static final String BASE_URL = "baseUrl";
    private String mainUrl;

    public DownloadService() {
        super(DownloadService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        mainUrl = intent != null ? intent.getStringExtra(BASE_URL) : null;
        Retrofit retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                .baseUrl(mainUrl)
                .build();

        HttpService httpService = retrofit.create(HttpService.class);
        httpService.listUsers().enqueue(new Callback<User[]>() {
            @Override
            public void onResponse(Call<User[]> call, Response<User[]> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    publishResult(response.body());
                }
            }

            @Override
            public void onFailure(Call<User[]> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void publishResult(User[] users) {
        if (users != null) {
            Toast.makeText(this, "Data has been found, size : " + users.length, Toast.LENGTH_SHORT).show();

            Intent notify = new Intent(NOTIFICATION);
            notify.putExtra(DATA, users);
            sendBroadcast(notify);
        }
    }
}
