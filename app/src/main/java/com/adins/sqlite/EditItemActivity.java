package com.adins.sqlite;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.adins.sqlite.adapter.UserAdapter;
import com.adins.sqlite.model.User;

import org.w3c.dom.Text;

public class EditItemActivity extends AppCompatActivity {

    TextView nama;
    EditText name;
    EditText email;
    EditText phone;
    Button updateButton;
    Button deleteButton;

    public static final String
            ACTION_USER_BROADCAST = EditItemActivity.class.getName() + "UserBroadcast",
            EXTRA_ID = "id",
            EXTRA_NAME = "name",
            EXTRA_EMAIL = "email",
            EXTRA_PHONE = "phone",
            EXTRA_POSITION = "position";

    int position;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_item);

        // initialize content
        nama = (TextView) findViewById(R.id.text);
        name = (EditText) findViewById(R.id.nameEdit);
        email = (EditText) findViewById(R.id.emailEdit);
        phone = (EditText) findViewById(R.id.phoneEdit);
        updateButton = (Button) findViewById(R.id.updateBtn);
        deleteButton = (Button) findViewById(R.id.deleteBtn);

        // get data position from intent
        intent = getIntent();
        position = intent.getIntExtra("position", 0);
//        nama.setText(intent.getStringExtra("name"));
    }

    @Override
    protected void onResume() {
        super.onResume();

        name.setText(intent.getStringExtra("name"));
        email.setText(intent.getStringExtra("email"));
        phone.setText(intent.getStringExtra("phone"));
    }

    public void updateButtonPress(View v) {

        // start broadcast message for update item
        Intent intent = new Intent(ACTION_USER_BROADCAST);
        intent.putExtra(EXTRA_ID, 1);
        intent.putExtra(EXTRA_NAME, name.getText().toString());
        intent.putExtra(EXTRA_EMAIL, email.getText().toString());
        intent.putExtra(EXTRA_PHONE, phone.getText().toString());
        intent.putExtra(EXTRA_POSITION, position);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

        finish();
    }

    public void deleteButtonPress(View v) {

        // start broadcast message for delete item
        Intent intent = new Intent(ACTION_USER_BROADCAST);
        intent.putExtra(EXTRA_ID, 2);
        intent.putExtra(EXTRA_POSITION, position);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

        finish();
    }

    public void addButtonPress(View v) {
        // start broadcast message for add item
        Intent intent = new Intent(ACTION_USER_BROADCAST);
        intent.putExtra(EXTRA_ID, 3);
        intent.putExtra(EXTRA_NAME, name.getText().toString());
        intent.putExtra(EXTRA_EMAIL, email.getText().toString());
        intent.putExtra(EXTRA_PHONE, phone.getText().toString());
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

        finish();
    }
}
