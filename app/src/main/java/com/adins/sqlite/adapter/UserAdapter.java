package com.adins.sqlite.adapter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.adins.sqlite.EditItemActivity;
import com.adins.sqlite.R;
import com.adins.sqlite.model.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by kusnendi on 4/3/18.
 */

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder> {
    private List<User> users;
    private Context context;

    public UserAdapter(Context context, List<User> users) {
        this.context = context;
        this.users   = users;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new UserViewHolder(LayoutInflater.from(context), parent);
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, final int position) {
        final User user = users.get(position);

        holder.fullname.setText(user.getName());
        holder.email.setText(user.getEmail());
        holder.phone.setText(user.getPhone());

        // set on click listener for specific item
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(context, user.getName(), Toast.LENGTH_LONG).show();
//                delete(position);

                // start edititem activity
                Intent intent = new Intent(context, EditItemActivity.class);
                intent.putExtra("position", position);
                intent.putExtra("name", user.getName());
                intent.putExtra("email", user.getEmail());
                intent.putExtra("phone", user.getPhone());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (users != null) ? users.size() : 0;
    }

    public static class UserViewHolder extends RecyclerView.ViewHolder {
        TextView fullname, email, phone;

        public UserViewHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.item_user, parent, false));

            fullname = itemView.findViewById(R.id.fullname);
            email    = itemView.findViewById(R.id.email);
            phone  = itemView.findViewById(R.id.phone);
        }
    }

    // update method for specific user item
    public void update(int position, String name, String email, String phone) {
        final User user = users.get(position);

        user.setName(name);
        user.setEmail(email);
        user.setPhone(phone);
        this.notifyDataSetChanged();
    }

    // delete method for specific user item
    public void delete(int position) {
        users.remove(position);
        this.notifyDataSetChanged();
    }

    // add method
    public void add(String name, String email, String phone) {
        User newUser = new User();

        newUser.setName(name);
        newUser.setEmail(email);
        newUser.setPhone(phone);

        users.add(newUser);
        this.notifyDataSetChanged();
    }
}
